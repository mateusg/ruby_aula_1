#1
puts 1+2

#2 -> Inteiros e floats
5
-205
9999999999999999999999999
0

54.321
0.001
-205.3884
0.0

#3 -> Aritmética simples

puts 1.0 + 2.0
puts 2.0 * 3.0
puts 5.0 - 8.0
puts 9.0 / 2.0

puts 1+2
puts 2*3
puts 5-8
puts 9/2 # Divisões com inteiros sempre retorna inteiro

puts 5 * (12-8) + -15
puts 98 + (59872 / (13*8)) * -52

